//exercise 1 - convert binary string to decimal

let a = '1001101'
let b = parseInt(a, 2);

console.log(b.toFixed());

//exercise 2 - convert binary string to decimal
let c  = '1.011'
let d  = parseInt(c, 2);

console.log(d.toFixed());

//exercise 3 - convert decimal string to binary
let e = '99'
let f = new Number(e).toString(2);

console.log(f);

//exercise 4 - convert decimal string to binary
let g = '0.8125'
let h = new Number(g).toString(2);

console.log(h);

//exercise 5 - Using the Math Object, draw a random card
//with a value between 1 & 13
//Draw 3 cards and determine the highest card

let max = '13'

function getRandomInt(max) {
    return Math.floor(Math.random() * 14);
}
let firstCard = getRandomInt()
let secondCard = getRandomInt()
let thirdCard = getRandomInt()

let highestCard = Math.max(firstCard, secondCard, thirdCard);

console.log("First Card = " + firstCard);
console.log("Second Card = " + secondCard);
console.log("Third Card = " + thirdCard);
console.log("Highest Card is " + highestCard);

//exercise 6 - cost per square inch for pagliacci pizza
const thirteenInchPrice = 16.99
const seventeenInchPrice = 19.99

let surfaceArea1 = Math.PI * 6.5**2
let surfaceArea2 = Math.PI * 8.5**2

const costPerSquareInch1 = thirteenInchPrice/surfaceArea1
const costPerSquareInch2 = seventeenInchPrice/surfaceArea2

console.log ('Cost Per Square Inch for 13 inch = ' + costPerSquareInch1 + ' cents');
console.log ('Cost Per Square Inch for 17 inch = ' + costPerSquareInch2 + ' cents');

//exercise 6 - string parsing
const address = 'Derek Ross\n3114 Emmaus Ave.\nZion, IL 60099'
console.log(address);

let firstSpace = address.indexOf(' ');
let endOfLine = address.indexOf('\n');
let firstNumber = address.search(/\d/);
let endOfStreetAddress = address.indexOf('.');
let cityStart = endOfStreetAddress + 3;
let stringLength = address.length
let endOfAddress = address.charAt(stringLength -1); 
let firstName = address.slice(0, firstSpace);
let lastName = address.slice(firstSpace + 1, endOfLine);
let streetAddress = address.slice(firstNumber, endOfStreetAddress + 1);
let cityStateZip = address.slice(cityStart, endOfAddress);

console.log(firstName);
console.log(lastName);
console.log(streetAddress);
console.log(cityStateZip);

//exercise 7 - find the middle date
const startDate = new Date(2019,0, 1);
const endDate = new Date(2019, 3, 1);
console.log(startDate);
console.log(endDate);
console.log('-----------------')
const midDate = new Date((startDate.getTime() + endDate.getTime()) / 2);
console.log(midDate);